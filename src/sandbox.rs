use crate::sandbox::data::Sandbox;
use std::collections::HashMap;
use std::fs::OpenOptions;
use std::io::Write;
use std::os::unix::prelude::*;
use std::path::{Path, PathBuf};
use std::process::{Child, Command, Stdio};
use std::{env, fs, io};
use uuid::Uuid;

#[cfg(feature = "nvidia")]
use crate::sandbox::gpu::nvidia;

#[cfg(feature = "pulseaudio")]
use crate::sandbox::audio::pulseaudio;

#[cfg(feature = "pipewire")]
use crate::sandbox::audio::pipewire;

#[cfg(feature = "wayland")]
use crate::sandbox::display::wayland;

#[cfg(feature = "x11")]
use crate::sandbox::display::x11;

pub(crate) mod data;

pub(crate) mod audio;
pub(crate) mod display;
pub(crate) mod gpu;
#[cfg(feature = "dbus")]
pub(crate) mod dbus;

pub const HOSTS_FILE: &str = "/etc/hosts";

impl Sandbox {
    pub fn add_custom_hosts(
        &mut self,
        entries: HashMap<String, Vec<String>>,
    ) -> Result<(), io::Error> {
        let mut hosts = PathBuf::from(HOSTS_FILE);
        let mut hosts_copied = PathBuf::from(format!("/tmp/hosts-{}", self.id.to_string()));

        fs::copy(&mut hosts, &mut hosts_copied)?;

        let mut arguments =
            Vec::from_iter(self.arguments.clone().into_iter().filter(|arg| match arg {
                BWrapArgument::Bind(_readonly, _skippable, _src, dst) => {
                    if dst.eq_ignore_ascii_case(HOSTS_FILE) {
                        false
                    } else {
                        true
                    }
                }
                _ => true,
            }));
        arguments.push(BWrapArgument::Bind(
            true,
            false,
            hosts_copied.to_str().unwrap().to_string(),
            String::from(HOSTS_FILE),
        ));
        self.arguments = arguments;

        let mut file = OpenOptions::new()
            .write(true)
            .append(true)
            .open(hosts_copied)?;

        file.write_all("\n\n".as_bytes())?;

        for (ip, hosts) in entries {
            file.write_all(
                format!(
                    "{} {}\n",
                    ip,
                    Vec::from_iter(hosts.clone().into_iter()).join(" ")
                )
                .as_bytes(),
            )?;
        }
        file.flush()?;

        Ok(())
    }

    pub fn add_argument(&mut self, arg: BWrapArgument) {
        self.arguments.push(arg);
    }

    fn build_sandbox_arguments(&self) -> Vec<String> {
        let mut args = Vec::new();

        for arg in self.arguments.clone() {
            args.append(&mut arg.to_string_vec());
        }

        args
    }

    pub fn spawn(
        self,
        mut cmd_args: Vec<String>,
        stdin: Option<Vec<u8>>,
    ) -> Result<Child, io::Error> {
        let mut args = Vec::new();
        let mut bwrap_args = self.build_sandbox_arguments();

        args.append(&mut bwrap_args);
        args.push(String::from("--"));
        args.append(&mut cmd_args);

        let child = Command::new("/usr/bin/bwrap")
            .stderr(Stdio::inherit())
            .stdout(Stdio::inherit())
            .stdin(if stdin.is_none() {
                Stdio::inherit()
            } else {
                Stdio::piped()
            })
            .args(args)
            .spawn()?;
        if let Some(stdin) = stdin {
            let mut stream = child.stdin.as_ref().unwrap();
            stream.write_all(stdin.as_slice())?;
            stream.flush()?;
        }
        Ok(child)
    }
}

#[derive(Clone)]
pub(crate) enum BWrapArgument {
    Bind(bool, bool, String, String),
    DevBind(String, String),
    Other(Vec<String>),
}

impl BWrapArgument {
    pub fn to_string_vec(&self) -> Vec<String> {
        match self {
            BWrapArgument::Bind(readonly, skippable, src, dst) => {
                let mut vec = Vec::new();
                if !(*skippable && !Path::new(src).exists()) {
                    vec.push(String::from(if *readonly { "--ro-bind" } else { "--bind" }));
                    vec.push(src.clone());
                    vec.push(dst.clone());
                }
                vec
            }
            BWrapArgument::DevBind(src, dst) => {
                let mut vec = Vec::new();
                vec.push(String::from("--dev-bind"));
                vec.push(src.clone());
                vec.push(dst.clone());
                vec
            }
            BWrapArgument::Other(argument) => argument.clone(),
        }
    }
}

impl Default for Sandbox {
    fn default() -> Sandbox {
        let uuid = Uuid::new_v4();
        let mut args: Vec<BWrapArgument> = Vec::new();

        // System bindings
        args.push(BWrapArgument::Other(vec![
            String::from("--dev"),
            String::from("/dev"),
            String::from("--proc"),
            String::from("/proc"),
        ]));
        args.push(BWrapArgument::DevBind(
            String::from("/sys"),
            String::from("/sys"),
        ));

        // System files
        args.push(BWrapArgument::Bind(
            true,
            true,
            String::from("/etc/ld.so.cache"),
            String::from("/etc/ld.so.cache"),
        ));
        args.push(BWrapArgument::Bind(
            true,
            true,
            String::from("/etc/ld.so.conf"),
            String::from("/etc/ld.so.conf"),
        ));
        args.push(BWrapArgument::Bind(
            true,
            true,
            String::from("/etc/ld.so.conf.d/"),
            String::from("/etc/ld.so.conf.d/"),
        ));
        args.push(BWrapArgument::Bind(
            true,
            true,
            String::from("/etc/passwd"),
            String::from("/etc/passwd"),
        ));
        args.push(BWrapArgument::Bind(
            true,
            true,
            String::from("/etc/localtime"),
            String::from("/etc/localtime"),
        ));

        // Libs
        args.push(BWrapArgument::Bind(
            true,
            false,
            String::from("/usr"),
            String::from("/usr"),
        ));
        args.push(BWrapArgument::Bind(
            true,
            false,
            String::from("/opt"),
            String::from("/opt"),
        ));
        args.push(BWrapArgument::Bind(
            true,
            false,
            String::from("/lib"),
            String::from("/lib"),
        ));
        args.push(BWrapArgument::Bind(
            true,
            false,
            String::from("/bin"),
            String::from("/bin"),
        ));
        args.push(BWrapArgument::Bind(
            true,
            false,
            String::from("/sbin"),
            String::from("/sbin"),
        ));
        args.push(BWrapArgument::Bind(
            true,
            false,
            String::from("/lib64"),
            String::from("/lib64"),
        ));

        // Networking
        args.push(BWrapArgument::Bind(
            true,
            false,
            String::from("/etc/resolv.conf"),
            String::from("/etc/resolv.conf"),
        ));
        args.push(BWrapArgument::Bind(
            true,
            true,
            String::from("/etc/host.conf"),
            String::from("/etc/host.conf"),
        ));
        args.push(BWrapArgument::Bind(
            true,
            false,
            String::from(HOSTS_FILE),
            String::from(HOSTS_FILE),
        ));
        args.push(BWrapArgument::Bind(
            true,
            false,
            String::from("/etc/nsswitch.conf"),
            String::from("/etc/nsswitch.conf"),
        ));

        // SSL Certificates
        args.push(BWrapArgument::Bind(
            true,
            false,
            String::from("/etc/ssl/certs"),
            String::from("/etc/ssl/certs"),
        ));

        // GPU
        args.push(BWrapArgument::DevBind(
            String::from("/dev/dri"),
            String::from("/dev/dri"),
        ));
        #[cfg(feature = "nvidia")]
        nvidia::apply(&mut args);

        let xdg_runtime_dir = env::var("XDG_RUNTIME_DIR").expect("XDG_RUNTIME_DIR is not set!");

        // Display servers
        #[cfg(feature = "wayland")]
        wayland::apply(&mut args, &xdg_runtime_dir);
        #[cfg(feature = "x11")]
        x11::apply(&mut args, &xdg_runtime_dir);

        // Audio servers
        #[cfg(feature = "pipewire")]
        pipewire::apply(&mut args, &xdg_runtime_dir);
        #[cfg(feature = "pulseaudio")]
        pulseaudio::apply(&mut args, &xdg_runtime_dir);

        // Other
        #[cfg(feature = "dbus")]
        dbus::apply(&mut args, &xdg_runtime_dir);

        let fs = fs::metadata("/proc/self/").expect("Failed to retrieve user information!"); // This folder is always owned by the current user

        // Configuration
        args.push(BWrapArgument::Other(vec![
            String::from("--chdir"),
            String::from(env::current_dir().unwrap().to_str().unwrap()),
            String::from("--uid"),
            fs.uid().to_string(),
            String::from("--gid"),
            fs.gid().to_string(),
            String::from("--unshare-ipc"),
            String::from("--unshare-pid"),
            String::from("--die-with-parent"),
            String::from("--new-session"),
        ]));

        Sandbox {
            id: uuid,
            arguments: args,
        }
    }
}
