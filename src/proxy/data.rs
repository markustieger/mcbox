use axum_server::tls_rustls::RustlsConfig;

#[derive(Clone)]
pub(crate) struct Proxy {
    pub host: String,
    pub token: String,

    pub rustls: Option<RustlsConfig>,
}
