use crate::proxy::Proxy;
use crate::TOKEN_REPLACEMENT;
use axum::body::{Body, Bytes};
use axum::http::header::CONTENT_TYPE;
use axum::http::{HeaderMap, HeaderValue, StatusCode};
use axum::response::Response;
use log::{debug, warn};
use reqwest::header::{ACCEPT, AUTHORIZATION, COOKIE, USER_AGENT};
use reqwest::RequestBuilder;

pub(crate) fn validate_uri(host: &String) -> Result<(), StatusCode> {
    if super::HOSTS.contains(host) {
        Ok(())
    } else {
        debug!("Host is forbidden: {}", host.clone());
        Err(StatusCode::FORBIDDEN)
    }
}

pub(crate) fn with_headers(
    mut builder: RequestBuilder,
    headers: &HeaderMap,
    state: &Proxy,
) -> RequestBuilder {
    builder = builder
        .header(
            ACCEPT,
            reqwest::header::HeaderValue::from_bytes(
                headers.get(ACCEPT.as_str()).unwrap().as_bytes(),
            )
            .unwrap(),
        )
        .header(
            USER_AGENT,
            reqwest::header::HeaderValue::from_bytes(
                headers.get(USER_AGENT.as_str()).unwrap().as_bytes(),
            )
            .unwrap(),
        );

    for name in vec![AUTHORIZATION, COOKIE] {
        if let Some(header) = headers.get(name.as_str()) {
            builder = builder.header(
                name,
                reqwest::header::HeaderValue::from_str(
                    header
                        .to_str()
                        .unwrap()
                        .replace(TOKEN_REPLACEMENT, state.token.as_str())
                        .as_str(),
                )
                .unwrap(),
            );
        }
    }

    builder
}

pub(crate) async fn convert_response(
    response: Result<reqwest::Response, reqwest::Error>,
) -> Result<Response, StatusCode> {
    match response {
        Ok(response) => {
            let mut header = None;
            if let Some(content_type) = response.headers().get(CONTENT_TYPE.as_str()) {
                header = Some(HeaderValue::from_bytes(content_type.as_bytes()).unwrap());
            }
            let status_code = response.status().as_u16();

            let bytes = response.bytes().await.unwrap_or(Bytes::new());

            let mut resp = Response::builder().body(Body::from(bytes)).unwrap();

            if let Some(header) = header {
                resp.headers_mut().insert(CONTENT_TYPE, header);
            }
            *resp.status_mut() = StatusCode::from_u16(status_code).unwrap();

            Ok(resp)
        }
        Err(err) => {
            warn!("Error happend in proxy: {}", err);
            Err(StatusCode::INTERNAL_SERVER_ERROR)
        }
    }
}
