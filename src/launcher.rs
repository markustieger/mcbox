use crate::sandbox::data::Sandbox;
use crate::sandbox::BWrapArgument;
use crate::sandbox::BWrapArgument::{Bind};
use crate::TOKEN_REPLACEMENT;



use std::collections::HashMap;

use std::io::ErrorKind;
use std::path::PathBuf;
use std::{env, fs, io};

pub(crate) enum Launcher {
    VANILLA,

    #[cfg(feature = "multimc")]
    MULTIMC {
        inst_dir: PathBuf,
        cmdline: HashMap<String, Vec<String>>,
    },
}

impl Launcher {
    pub async fn initialize(&mut self) -> Result<(), io::Error> {
        match self {
            Launcher::VANILLA => Ok(()),
            #[cfg(feature = "multimc")]
            Launcher::MULTIMC { inst_dir, cmdline } => {
                *cmdline = multimc_read_cmdline()?;
                *inst_dir = PathBuf::from(env::var("INST_DIR").unwrap_or(String::from("..")));
                Ok(())
            }
        }
    }

    pub async fn accesstoken(&mut self, mut cmd: &mut Vec<String>) -> Option<String> {
        #[cfg(feature = "multimc")]
        if let Launcher::MULTIMC { cmdline, .. } = self {
            cmd = cmdline.get_mut(&String::from("param")).unwrap();
        }
        accesstoken_generic(cmd)
    }

    pub async fn stdin(&mut self) -> Option<Vec<u8>> {
        match self {
            Launcher::VANILLA => None,
            #[cfg(feature = "multimc")]
            Launcher::MULTIMC { inst_dir: _, cmdline } => {
                let mut data = String::new();
                for (key, values) in cmdline.clone() {
                    for value in values {
                        data += format!("{} {}\n", key, value).as_str();
                    }
                }
                data += "launch\n";
                Some(data.into_bytes())
            }
        }
    }

    pub fn apply(&mut self, sandbox: &mut Sandbox, cmd: &mut Vec<String>) -> Result<(), io::Error> {
        match self {
            Launcher::VANILLA => {
                let jvm_home = env::var("MCBOX_JAVA_HOME")
                    .unwrap_or(env::var("JAVA_HOME").unwrap_or(String::from("/usr")));
                let jvm_home = PathBuf::from(jvm_home);
                cmd.insert(0, jvm_home.join("bin/java").to_str().unwrap().to_string());

                let folder = String::from(env::current_dir().unwrap().to_str().unwrap());
                sandbox.add_argument(BWrapArgument::Bind(false, false, folder.clone(), folder));
            }
            #[cfg(feature = "multimc")]
            Launcher::MULTIMC { inst_dir, .. } => {
                let data = String::from(inst_dir.clone().to_str().unwrap());
                let assets = String::from(inst_dir.join("../../assets").to_str().unwrap());
                let assets_skins =
                    String::from(inst_dir.join("../../assets/skins/").to_str().unwrap());

                let libraries_path = inst_dir.join("../../libraries");
                let libraries = String::from(libraries_path.to_str().unwrap());

                sandbox.add_argument(Bind(true, false, assets.clone(), assets));
                sandbox.add_argument(Bind(false, false, assets_skins.clone(), assets_skins));

                if env::var("MCBOX_WRITABLE_LIBRARIES")
                    .unwrap_or(false.to_string())
                    .eq_ignore_ascii_case(true.to_string().as_str())
                {
                    if env::var("MCBOX_WRITABLE_PERSISTENT_LIBRARIES")
                        .unwrap_or(false.to_string())
                        .eq_ignore_ascii_case(true.to_string().as_str())
                    {
                        let libraries_writable = inst_dir.join("persistent_libraries");
                        fs::create_dir_all(libraries_writable.clone())?;

                        sandbox.add_argument(Bind(
                            false,
                            false,
                            String::from(libraries_writable.to_str().unwrap()),
                            libraries.clone(),
                        ));
                    }

                    let files = visit_dirs(libraries_path.clone())?;

                    for file in files {
                        let path = String::from(file.to_str().unwrap());
                        sandbox.add_argument(Bind(true, false, path.clone(), path));
                    }
                } else {
                    sandbox.add_argument(Bind(true, false, libraries.clone(), libraries));
                }
                sandbox.add_argument(Bind(false, false, data.clone(), data));
            }
        };
        Ok(())
    }
}

fn visit_dirs(dir: PathBuf) -> Result<Vec<PathBuf>, io::Error> {
    let mut files = Vec::new();
    if dir.is_dir() {
        for entry in fs::read_dir(dir)? {
            let entry = entry?;
            let path = entry.path();
            if path.is_dir() {
                files.append(&mut visit_dirs(path)?);
            } else {
                files.push(path);
            }
        }
    }
    Ok(files)
}

fn accesstoken_generic(cmdline: &mut Vec<String>) -> Option<String> {
    let mut token_next = false;

    let mut i = 0;
    while i < cmdline.len() {
        let arg = &cmdline[i];

        if token_next {
            let token = cmdline[i].clone();
            cmdline[i] = String::from(TOKEN_REPLACEMENT);
            return Some(token);
        }
        if arg.eq_ignore_ascii_case("--accessToken") {
            token_next = true;
        }

        i += 1
    }

    None
}

pub(crate) fn detect_launcher() -> Launcher {
    #[cfg(feature = "multimc")]
    if let Ok(_inst_dir) = env::var("INST_DIR") {
        return Launcher::MULTIMC {
            inst_dir: PathBuf::from(String::from("..")),
            cmdline: HashMap::new(),
        };
    }
    Launcher::VANILLA
}

#[cfg(feature = "multimc")]
fn multimc_read_cmdline() -> Result<HashMap<String, Vec<String>>, io::Error> {
    let mut buffer = String::new();

    let mut current = MultiMCStatus::PROCEED;
    let mut args: HashMap<String, Vec<String>> = HashMap::new();

    while current == MultiMCStatus::PROCEED {
        let bytes = io::stdin().read_line(&mut buffer).unwrap();
        let buf = &buffer[buffer.len() - bytes..];

        let mut buffer = String::from(buf);

        if buffer.ends_with("\n") {
            buffer.pop();
        }
        if buffer.ends_with(" ") {
            buffer.pop();
        }

        current = match buffer.as_str() {
            "" => MultiMCStatus::PROCEED,
            "launch" => MultiMCStatus::LAUNCH,
            "abort" => MultiMCStatus::ABORT,
            _ => {
                if let Some((key, value)) = buffer.split_once(" ") {
                    let key = String::from(key);

                    if !args.contains_key(&key) {
                        args.insert(key.clone(), Vec::new());
                    }
                    args.get_mut(&key).unwrap().push(String::from(value));
                }

                MultiMCStatus::PROCEED
            }
        }
    }

    if current == MultiMCStatus::ABORT {
        return Err(io::Error::new(ErrorKind::Other, "Aborted by MultiMC!"));
    }

    Ok(args)
}

#[cfg(feature = "multimc")]
#[derive(PartialEq)]
enum MultiMCStatus {
    PROCEED,
    ABORT,
    LAUNCH,
}
