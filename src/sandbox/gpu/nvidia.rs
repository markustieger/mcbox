use crate::sandbox::BWrapArgument;
use crate::sandbox::BWrapArgument::*;
use std::fs;

pub(crate) fn apply(args: &mut Vec<BWrapArgument>) {
    let dir = fs::read_dir("/dev").expect("Failed to read /dev !");
    for entry in dir {
        if entry.is_err() {
            continue;
        }
        let entry = entry.unwrap();
        let filename = entry.file_name();
        let filename = filename.to_str().unwrap();
        if filename.starts_with("nvidia") {
            let path = String::from(entry.path().to_str().unwrap());
            args.push(DevBind(path.clone(), path));
        }
    }
}
