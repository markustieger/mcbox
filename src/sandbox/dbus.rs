use crate::sandbox::BWrapArgument;
use crate::sandbox::BWrapArgument::Bind;

pub(crate) fn apply(args: &mut Vec<BWrapArgument>, xdg_runtime_dir: &String) {
    let socket = format!(
        "{}/bus",
        xdg_runtime_dir,
    );

    args.push(Bind(false, true, socket.clone(), socket));
}
