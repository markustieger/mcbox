use crate::sandbox::BWrapArgument;
use crate::sandbox::BWrapArgument::*;
use std::env;

pub(crate) fn apply(args: &mut Vec<BWrapArgument>, xdg_runtime_dir: &String) {
    let socket = format!(
        "{}/{}",
        xdg_runtime_dir,
        env::var("WAYLAND_DISPLAY").unwrap_or(String::from("wayland-1"))
    );

    args.push(Bind(false, true, socket.clone(), socket));
}
