use crate::sandbox::BWrapArgument;
use crate::sandbox::BWrapArgument::*;
use std::env;

pub(crate) fn apply(args: &mut Vec<BWrapArgument>, _xdg_runtime_dir: &String) {
    let unix_sockets = String::from("/tmp/.X11-unix/");
    let xauthority = format!(
        "{}/.Xauthority",
        env::var("HOME").expect("HOME is not set!")
    );

    args.push(Bind(false, true, unix_sockets.clone(), unix_sockets));
    args.push(Bind(true, true, xauthority.clone(), xauthority));
}
