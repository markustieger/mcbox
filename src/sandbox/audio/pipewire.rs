use crate::sandbox::BWrapArgument;
use crate::sandbox::BWrapArgument::*;

pub(crate) fn apply(args: &mut Vec<BWrapArgument>, xdg_runtime_dir: &String) {
    let socket = format!("{}/pipewire-0", xdg_runtime_dir);

    args.push(Bind(true, true, socket.clone(), socket));
}
