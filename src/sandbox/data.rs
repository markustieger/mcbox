use crate::sandbox::BWrapArgument;
use uuid::Uuid;

pub(crate) struct Sandbox {
    pub(crate) id: Uuid,
    pub(crate) arguments: Vec<BWrapArgument>,
}
