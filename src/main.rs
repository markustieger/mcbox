pub(crate) mod launcher;
pub(crate) mod proxy;
pub(crate) mod sandbox;

use crate::proxy::data::Proxy;
use crate::sandbox::data::Sandbox;


use log::{debug, info, LevelFilter};









use std::process::{ExitCode};
use std::string::String;
use std::time::Duration;
use std::{env};





const TOKEN_REPLACEMENT: &str =
    "// Seems like you did not get my token //";

#[tokio::main]
pub async fn main() -> ExitCode {
    env_logger::builder()
        .filter_level(LevelFilter::Info)
        .parse_default_env()
        .init();

    let mut cmdline = Vec::from_iter(env::args());
    cmdline.remove(0); // own file

    let mut launcher = launcher::detect_launcher();

    launcher
        .initialize()
        .await
        .expect("Failed to initialize launcher-specific code!");

    let token = launcher.accesstoken(&mut cmdline).await;

    let mut sandbox = Sandbox::default();

    debug!("Sandbox ID: {}", sandbox.id.to_string());

    launcher
        .apply(&mut sandbox, &mut cmdline)
        .expect("Failed to apply launcher-specific code!");

    if let Some(token) = token {
        let mut proxy = Proxy {
            token,
            host: String::new(),
            rustls: None,
        };

        proxy
            .prepare(&mut sandbox, &mut cmdline)
            .await
            .expect("Failed to prepare proxy!");

        proxy.start().await.expect("Failed to start proxy!");
    } else {
        info!("No token could be found! Start continues in 30 seconds...");
        tokio::time::sleep(Duration::from_secs(30)).await;
    }

    if let Ok(wrapper) = env::var("MCBOX_LAUNCH_WRAPPER") {
        let mut vec: Vec<String> = wrapper.split(";").map(String::from).collect();
        vec.append(&mut cmdline);
        cmdline = vec;
    }

    let mut child = sandbox
        .spawn(cmdline, launcher.stdin().await)
        .expect("Failed to spawn sandbox!");

    let code = ExitCode::from(
        child
            .wait()
            .expect("Failed to wait for child to finish!")
            .code()
            .unwrap() as u8,
    );

    code
}
