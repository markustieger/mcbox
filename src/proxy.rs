pub(crate) mod data;
mod utils;

use crate::proxy::data::*;
use crate::sandbox::data::Sandbox;
use crate::sandbox::BWrapArgument::Bind;
use crate::TOKEN_REPLACEMENT;
use axum::extract::State;
use axum::http::header::HOST;
use axum::http::{HeaderMap, Method, StatusCode, Uri};
use axum::response::{IntoResponse};
use axum::Router;
use axum_server::tls_rustls::RustlsConfig;
use lazy_static::lazy_static;
use rcgen::{BasicConstraints, CertificateParams, IsCa, KeyPair, KeyUsagePurpose};
use std::collections::{HashMap, HashSet};
use std::fs::File;
use std::io::{ErrorKind, Write};
use std::os::unix::prelude::*;
use std::path::{Path, PathBuf};
use std::process::{Command};
use std::str::FromStr;
use std::{env, fs, io};


lazy_static! {
    pub static ref HOSTS: HashSet<String> = HashSet::from([
        String::from("sessionserver.mojang.com"),
        String::from("api.minecraftservices.com"),
        String::from("pc.realms.minecraft.net"),
        String::from("authserver.mojang.com"),
        String::from("session.minecraft.net"),
        String::from("api.mojang.com")
    ]);
}

impl Proxy {
    pub async fn start(self) -> Result<(), io::Error> {
        let _ = tokio::task::spawn(async move {
            self.start0().await;
        });
        Ok(())
    }

    pub async fn start0(self) {
        let host = self.host.clone();
        let rustls_config = self.rustls.clone();

        let app = Router::new().fallback(handle).with_state(self);

        let listener = std::net::TcpListener::bind(format!("{}:443", host))
            .expect("Failed to bind to address! Maybe CAP_NET_BIND_SERVICE missing?");
        let listener = axum_server::from_tcp_rustls(listener, rustls_config.unwrap());
        listener
            .serve(app.into_make_service())
            .await
            .expect("Failed to serve proxy!");
    }

    pub async fn prepare(
        &mut self,
        sandbox: &mut Sandbox,
        cmdline: &mut Vec<String>,
    ) -> Result<(), io::Error> {
        self.host = format!("127.0.10.{}", 1 + (rand::random::<u8>() % 200));

        let mut ca_certs_copied =
            PathBuf::from(format!("/tmp/keystore-{}", sandbox.id.to_string()));
        let path = String::from(ca_certs_copied.to_str().unwrap());

        sandbox.add_argument(Bind(true, false, path.clone(), path));

        let mut params = CertificateParams::new(Vec::from_iter(HOSTS.clone().into_iter())).map_err(|err| io::Error::new(ErrorKind::Other, err))?;
        params.is_ca = IsCa::Ca(BasicConstraints::Constrained(3));

        params.key_usages = vec![
            KeyUsagePurpose::DigitalSignature,
            KeyUsagePurpose::KeyCertSign,
        ];
        let keypair = KeyPair::generate().map_err(|err| io::Error::new(ErrorKind::Other, err))?;
        let certificate = params.self_signed(&keypair).map_err(|err| io::Error::new(ErrorKind::Other, err))?;

        let config = RustlsConfig::from_pem(
            certificate.pem().as_bytes().to_vec(),
            keypair.serialize_pem().as_bytes().to_vec(),
        )
        .await
        .unwrap();
        self.rustls = Some(config);

        let java = cmdline
            .get(0)
            .expect("Failed to find the java binary you are executing!");
        let java = Path::new(java);

        let env = env::var("JAVA_TRUST_STORE");
        let java_home = java.parent().unwrap().join("..");

        let mut cacerts = if !env.clone().unwrap_or(String::new()).is_empty() {
            PathBuf::from(env.unwrap())
        } else {
            java_home.join("lib/security/cacerts")
        };

        if !cacerts.exists() {
            return Err(io::Error::new(
                ErrorKind::Other,
                "Couldn't find trust-store!",
            ));
        }

        let cert = PathBuf::from(format!("/tmp/cert-{}", sandbox.id.to_string()));

        let mut file = File::create(cert.clone()).unwrap();
        file.write_all(certificate.pem().as_bytes())
            .expect("Failed to write cert!");
        file.flush().expect("Failed to flush cert!");

        fs::copy(&mut cacerts, &mut ca_certs_copied).expect("Failed to copy keystore!");

        let metadata =
            fs::metadata(ca_certs_copied.clone()).expect("Failed to get metadata of keystore.");
        let mut perms = metadata.permissions();
        perms.set_mode(0o777);
        fs::set_permissions(ca_certs_copied.clone(), perms).expect("Failed to set permissions!");

        let keytool = java.parent().unwrap().join("keytool");

        // keytool -import -alias ca -file somecert.cer -keystore cacerts -storepass changeit -noprompt

        let exit = Command::new(keytool.to_str().unwrap())
            .arg("-import")
            .arg("-alias")
            .arg("minecraft_ca")
            .arg("-trustcacerts")
            .arg("-file")
            .arg(cert.to_str().unwrap())
            .arg("-keystore")
            .arg(ca_certs_copied.to_str().unwrap())
            .arg("-storepass")
            .arg("changeit") // Default pwd
            .arg("-noprompt")
            .spawn()?
            .wait()?
            .code()
            .unwrap();
        if exit != 0 {
            return Err(io::Error::new(
                ErrorKind::Other,
                "Couldn't inject key into truststore!",
            ));
        }

        cmdline.insert(
            1,
            format!(
                "-Djavax.net.ssl.trustStore={}",
                ca_certs_copied.to_str().unwrap()
            ),
        );
        cmdline.insert(
            1,
            String::from("-Djavax.net.ssl.trustStorePassword=changeit"),
        );
        // args_mapped.insert(1, String::from("-Djavax.net.debug=all"));
        cmdline.insert(1, String::from("-Djavax.net.ssl.trustStoreType=jks"));

        sandbox.add_custom_hosts(HashMap::from([(
            self.host.clone(),
            Vec::from_iter(HOSTS.clone().into_iter()),
        )]))?;

        Ok(())
    }

    pub async fn handle(
        self,
        method: Method,
        uri: Uri,
        headers: HeaderMap,
        mut body: String,
    ) -> impl IntoResponse {
        let host = headers
            .get(HOST)
            .map(|header| header.to_str().unwrap().to_string())
            .unwrap_or(String::from("unknown.local"))
            .trim()
            .to_string();

        utils::validate_uri(&host)?;

        let client = reqwest::Client::builder().build().unwrap();

        let method = reqwest::Method::from_str(method.as_str())
            .map_err(|_| StatusCode::METHOD_NOT_ALLOWED)?;
        match method {
            reqwest::Method::CONNECT => {
                return Err(StatusCode::METHOD_NOT_ALLOWED);
            }
            _ => {}
        }

        body = body.replace(TOKEN_REPLACEMENT, self.token.clone().as_str());

        let mut request = client.request(method, format!("https://{}{}", host, uri.path()));
        request = utils::with_headers(request, &headers, &self);
        let request = request
            .body(body)
            .build()
            .expect("Failed to build request!");

        let response = client.execute(request).await;
        utils::convert_response(response).await
    }
}

async fn handle(
    method: Method,
    uri: Uri,
    headers: HeaderMap,
    State(proxy): State<Proxy>,
    body: String,
) -> impl IntoResponse {
    proxy.handle(method, uri, headers, body).await
}
